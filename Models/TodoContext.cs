using Microsoft.EntityFrameworkCore;

namespace TDApi.Models
{
    public class TDContext : DbContext
    {
        public TDContext(DbContextOptions<TDContext> options)
            : base(options)
        {

        }
        public DbSet<TDItem> TDItems { get; set; }
    }
}