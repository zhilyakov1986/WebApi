using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using TDApi.Models;


namespace TDApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TDController : ControllerBase
    {
        private readonly TDContext _context;
        public TDController(TDContext context)
        {
            _context = context;
            if (_context.TDItems.Count() == 0)
            {
                _context.TDItems.Add(new TDItem { Name = "Item1" });
                _context.SaveChanges();
            }
        }
        [HttpGet]
        public ActionResult<List<TDItem>> GetAll()
        {
            return _context.TDItems.ToList();
        }

        [HttpGet("{id}", Name = "GetTD")]
        public ActionResult<TDItem> GetById(long id)
        {
            var item = _context.TDItems.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

    }
}